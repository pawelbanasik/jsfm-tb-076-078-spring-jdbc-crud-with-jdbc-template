package com.pawelbanasik.springdemo.daoimpl;

import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import com.pawelbanasik.springdemo.dao.OrganizationDao;
import com.pawelbanasik.springdemo.domain.Organization;

// @Component would also work but @Repository is advised
@Repository("orgDao")
public class OrganizationDaoImpl implements OrganizationDao {

	private JdbcTemplate jdbcTemplate;

	@Autowired
	public void setDataSource(DataSource dataSource) {
		jdbcTemplate = new JdbcTemplate(dataSource);
	}

	// prepared statement
	// DTO
	public boolean create(Organization org) {
		String sqlQuery = "INSERT INTO organization (company_name, year_of_incorporation, "
				+ "postal_code, employee_count, slogan) " + "VALUES(?, ?, ?, ?, ?)";
		Object[] args = new Object[] { org.getCompanyName(), org.getYearOfIncorporation(), org.getPostalCode(),
				org.getEmployeeCount(), org.getSlogan() };
		// returns the numbers of rows affected - check doc
		return jdbcTemplate.update(sqlQuery, args) == 1;
	}

	public Organization getOrganization(Integer id) {
		String sqlQuery = "SELECT id, company_name, year_of_incorporation, postal_code, "
				+ " employee_count, slogan FROM organization WHERE id = ?";
		Object[] args = new Object[] { id };
		Organization org = jdbcTemplate.queryForObject(sqlQuery, args, new OrganizationRowMapper());
		return org;
	}

	// here we use the organization row mapper!
	public List<Organization> getAllOrganizations() {
		String sqlQuery = "SELECT * FROM organization";
		List<Organization> orgList = jdbcTemplate.query(sqlQuery, new OrganizationRowMapper());

		return orgList;
	}

	public boolean delete(Organization org) {
		String sqlQuery = "DELETE FROM organization WHERE id = ?";
		Object[] args = new Object[] { org.getId() };
		return jdbcTemplate.update(sqlQuery, args) == 1;
	}

	public boolean update(Organization org) {
		String sqlQuery = "UPDATE organization SET slogan = ? WHERE id = ?";
		Object[] args = new Object[] { org.getSlogan(), org.getId() };
		return jdbcTemplate.update(sqlQuery, args) == 1;
	}

	// normally not a part of dao; this is for presentation
	// this will delete all data from the table and reset the autoincrement
	// execute often used for DDL Statements - data definiton language
	// there is also DML - data manipulation language
	// many variants of execute methods - worth to check it out
	public void cleanup() {
		// deletes all in table but also resets the autoincrement
		String sqlQuery = "TRUNCATE TABLE organization ";
		jdbcTemplate.execute(sqlQuery);

	}

}
